var ballx =Math.floor(Math.random() * 300) + 50;//randomises the starting position of the ball on the x axis
var bally=50;//balls starting coordinates on the y axis
//allows the ball to continously change direction, also increasing the ballchane incxreases the speed
var yballchange = 5;
var xballchange= 5;
var diameter = 50;//size of ball
//positions of the paddles and its width
var paddlex;
var paddley;
var paddleWidth = 150;
var started = false;//when changed to true stops the code related to it from running again
//sets the starting score to 0
var score = 0;

//Sets the background colour when the program is run
function setup() {
  createCanvas(700, 500);
}

function draw() {
  background(0);
  //sets ball colour, size and start coordinates
  fill(255,0,0)
  ellipse(ballx,bally,diameter,diameter)
  
  //this causes the ball to move on the screen screen
  ballx += xballchange;
  bally += yballchange;
  //ball bounces, the -x/y means that when the ball hits the sides of the box the ball will go in the opposite direction
  if (ballx<diameter/2||
      ballx > 700- diameter){xballchange *=-1;}
      if (bally<diameter/2||
      bally > 500 - diameter){yballchange *=-1;}
  
  //ball and paddle collistion
  // if the ball hits the paddle then the balls direction will change as well as increase the score 
  if ((ballx > paddlex &&
       ballx< paddlex+ paddleWidth&&
       bally + (diameter/2) >= paddley)){
    xballchange *=-1;
    yballchange *=-1;
    score++;
  }
  //sets paddles starting position so it starts in the same position each time
  if (!started) {
  paddlex = 300;
  paddley = 400;
  started = true;//when set to true stops this section of code from running again
}
  //creates paddle
  fill(0, 255, 0);
noStroke();
rect(paddlex, paddley, 150, 25);
  
  //score display
  fill(255,255,255);
  textSize(20);
  text("Score: " + score, 10, 25);
}
//function that moves the paddle with the arrow keys
function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    paddlex -= 50;
  } else if (keyCode === RIGHT_ARROW) {
    paddlex += 50;
  }
}

